<?php
// $Id: youtube_api.module,v 1.13 2008/07/23 05:04:01 beeradb Exp $

/**
 * @file
 * Provides integration to the YouTube Public API.
 */

function youtube_playlist_snippet_attributes() {
  return array(
    'title' => 'setTitle',
    'description' => 'setDescription',
    'tags'  => 'setTags',
  );
}

function youtube_playlist_status_attributes() {
  return array(
    'privacyStatus' => 'setPrivacyStatus',
  );
}

function youtube_video_snippet_attributes() {
  return array(
    'title' => 'setTitle',
    'description' => 'setDescription',
    'tags'  => 'setTags',
    'categoryId' => 'setCategoryId',
  );
}

function youtube_video_status_attributes() {
  return array(
    'privacyStatus' => 'setPrivacyStatus',
    'embeddable'    => 'setEmbeddable',
    'license'       => 'setLicense',
    'publicStatsViewable' => 'setPublicStatsViewable',
  );
}

/**
  *  * Implements hook_libraries_info_alter().
  *   */
function youtube_api_libraries_info_alter(&$libraries) {
  $libraries['google-api-php-client']['files']['php'][] = 'src/contrib/Google_YouTubeService.php';
  $libraries['google-api-php-client']['versions']['google-api-php-client/0.6.5'] = array();
}

function youtube_api_youtube_object($account_id, $by_name = TRUE) {
  $info = libraries_load("google-api-php-client");
  if (!$info['loaded']) {
    drupal_set_message(t("Can't perform any operation as library is missing check Status report or Readme for requirements"), 'error');
    return FALSE;
  }
  $google_client = gauth_client_get($account_id, $by_name);
  if (!$google_client) {
    drupal_set_message(t("Can't create google client object, account is Invalid"), 'error');
    return FALSE;
  }
  return new Google_YouTubeService($google_client);
}
/**
 * Function for themeing and sending the upload request for youtube. Originally made
 * a theme function in case changes to youtubes API move faster than changes to this module
 * Should be moved to a regular function, as I think this will be stable between API changes anyway,
 * and we're more likely to see changes to the XML specification.
 *
 * @param $client
 *   The Google_Client object of the authenticated user.
 *
 * @param $file
 *   The file object on the server to the media being uploaded.
 *
 * @param $params
 *   Array that contains the video title, description, tags, etc. as they will appear on youtube.

You can set values for these properties:

    snippet.title
    snippet.description
    snippet.tags[]
    snippet.categoryId
    status.privacyStatus
    status.embeddable
    status.license
    status.publicStatsViewable

 */
function youtube_api_video_insert($filepath, $params, $account_id, $by_name = TRUE) {
  $youtube = youtube_api_youtube_object($account_id, $by_name);

  try {
    $video = new Google_Video();

    $parts = array();
    // Create a snippet with title, description, tags and category ID
    // Create an asset resource and set its snippet metadata and type.
    // This example sets the video's title, description, keyword tags, and
    // video category.
    //
    if (array_key_exists('snippet', $params)) {
      $attributes = youtube_video_snippet_attributes();
      // Clean array before passing it if user has passed any unwanted indexes.
      $params['snippet'] = array_intersect_key($params['snippet'], $attributes);
      $videoSnippet = new Google_VideoSnippet();
      foreach ($params['snippet'] as $attr => $val) {
        $videoSnippet->$attributes[$attr]($val);
      }
      $video->setSnippet($videoSnippet);
      $parts[] = 'snippet';
    }

    if (array_key_exists('status', $params)) {
      $attributes = youtube_video_status_attributes();
      // Clean array before passing it if user has passed any unwanted indexes.
      $params['status'] = array_intersect_key($params['status'], $attributes);
      $videoStatus = new Google_VideoStatus();
      foreach ($params['status'] as $attr => $val) {
        $videoStatus->$attributes[$attr]($val);
      }
      $video->setStatus($videoStatus);
      $parts[] = 'status';
    }

    // Specify the size of each chunk of data, in bytes. Set a higher value for
    // reliable connection as fewer chunks lead to faster uploads. Set a lower
    // value for better recovery on less reliable connections.
    $chunkSizeBytes = $options['chunk_size'] ? $options['chunk_size'] : 1 * 1024 * 1024;

    // Setting the defer flag to true tells the client to return a request which can be called
    // with ->execute(); instead of making the API call immediately.
    //$client->setDefer(true);

    // Create a MediaFileUpload with resumable uploads
    $media = new Google_MediaFileUpload('video/*', null, true, $chunkSizeBytes);
    $media->setFileSize(filesize($filepath));

    $parts = implode(',', $parts);;
    // Create a video insert request
    $insertResponse = $youtube->videos->insert($parts, $video,
      array('mediaUpload' => $media));

    // Read the media file and upload it chunk by chunk.
    $status = false;
    $handle = fopen($filepath, "rb");
    while (!$status && !feof($handle)) {
      $chunk = fread($handle, $chunkSizeBytes);
      $status = $media->nextChunk($insertResponse, $chunk);
    }

    fclose($handle);

    return $status;

  } catch (Google_ServiceException $e) {
    watchdog('youtube_api', t('A service error occurred: :code', array(':code' => $e->getMessage())));
  } catch (Google_Exception $e) {
    watchdog('youtube_api', t('A client error occurred: :code', array(':code' => $e->getMessage())));
  }

  return FALSE;
}

/**
 * Pulls back a list of videos a specific user has uploaded to youtube.
 *
 * @param $client
 *   The Google_Client object of the authenticated user.
 *
 * @param $user
 *   The user who's feed you'd like. If none is supplied it will default to the currently logged in youtube user (youtube_api_login).
 *
 * t@TODO: use tokenization
 *
 */
function youtube_api_get_user_uploads($pageToken = 0, $maxResults = 10, $account_id, $by_name = TRUE) {
  $youtube = youtube_api_youtube_object($account_id, $by_name);
  try {
    // Call the channels.list method to retrieve information about the
    // currently authenticated user's channel.
    $channelsResponse = $youtube->channels->listChannels('contentDetails', array(
      'mine' => 'true',
    ));

    foreach ($channelsResponse['items'] as $channel) {
      // Extract the unique playlist ID that identifies the list of videos
      // uploaded to the channel, and then call the playlistItems.list method
      // to retrieve that list.
      $uploadsListId = $channel['contentDetails']['relatedPlaylists']['uploads'];

      $params = array(
        'playlistId' => $uploadsListId,
        'maxResults' => $maxResults, 
      );

      if ($pageToken) {
        $params['pageToken'] = $pageToken;
      }

      $playlistItemsResponse = $youtube->playlistItems->listPlaylistItems('snippet', $params);

      return $playlistItemsResponse;

    }
  } catch (Google_ServiceException $e) {
    watchdog('youtube_api', t('A service error occurred: :code', array(':code' => $e->getMessage())));
  } catch (Google_Exception $e) {
    watchdog('youtube_api', t('A client error occurred: :code', array(':code' => $e->getMessage())));
  }
  return FALSE;

}

/**
 * Parse the returns of a single video entry from the XML feed and return it as an object.
 *
 * @param $entry
 *   The simpleXML object for a video's entry record.
 *
 * @TODO: Probably needs some cleanup, I could be more proficient in SimpleXML. Would love
 * someone more proficient to give advice on approach here.
 */
function youtube_api_video_list($video_id, $parts = array('snippet'), $account_id, $by_name = TRUE) {
  $youtube = youtube_api_youtube_object($account_id, $by_name);


  try{

    // Create a video list request
    $listResponse = $youtube->videos->listVideos(implode(',', $parts),
      array('id' => $video_id));

    $videoList = $listResponse['items'];
    if (empty($videoList)) {
      watchdog('youtube_api', t('Can not find a video with id: :video_id', array(':video_id' => $videoId)));
    } else {
      // Since a unique video id is given, it will only return 1 video.
      $video = $videoList[0];
      return $video;
    }
  } catch (Google_ServiceException $e) {
    watchdog('youtube_api', t('A service error occurred: :code', array(':code' => $e->getMessage())));
  } catch (Google_Exception $e) {
    watchdog('youtube_api', t('A client error occurred: :code', array(':code' => $e->getMessage())));
  }

  return FALSE;

}

/**
 * A Function for updating video information.
 *
 * @param $xml
 *   The XML which contains new video information.
 * @param $video_id
 *  The id of the video you are updating.
 */
function youtube_api_video_update($video_id, $params, $account_id, $by_name = TRUE) {
  $youtube = youtube_api_youtube_object($account_id, $by_name);
  try{
    // REPLACE with the video ID that you want to update

    $video = youtube_api_video_list($video_id, array('snippet,status'));

    if ($video) {

      $updateVideo = new Google_Video($video);

      $snippet = array_key_exists('snippet', $params) ? $params['snippet'] : array();

      $snippet = array_merge($video['snippet'], $snippet);

      $updateVideo->setSnippet(new Google_VideoSnippet($snippet));

      $status = array_key_exists('status', $params) ? $params['status'] : array();

      $status = array_merge((array) $status, (array) ($video['status']));

      $updateVideo->setStatus(new Google_VideoStatus($snippet));

      // Create a video list request
      return $youtube->videos->update('snippet,status', $updateVideo);
    }

  } catch (Google_ServiceException $e) {
    watchdog('youtube_api', t('A service error occurred: :code', array(':code' => $e->getMessage())));
  } catch (Google_Exception $e) {
    watchdog('youtube_api', t('A client error occurred: :code', array(':code' => $e->getMessage())));
  }
  return FALSE;
}

/**
 * A Function for deleting a video.
 *
 * @param $video_id
 *   The id for the youtube video you'd like to delete.
 */
function youtube_api_video_delete($video_id, $account_id, $by_name = TRUE) {
  $youtube = youtube_api_youtube_object($account_id, $by_name);

  try{

    // Create a video list request
    return $youtube->videos->delete($videoId);

  } catch (Google_ServiceException $e) {
    watchdog('youtube_api', t('A service error occurred: :code', array(':code' => $e->getMessage())));
  } catch (Google_Exception $e) {
    watchdog('youtube_api', t('A client error occurred: :code', array(':code' => $e->getMessage())));
  }
  return FALSE;
}

/*


You must specify a value for these properties:

    snippet.title

You can set values for these properties:

    snippet.title
    snippet.description
    status.privacyStatus
    snippet.tags[]

*/
function youtube_api_playlist_insert($params, $account_id, $by_name = TRUE) {
  $youtube = youtube_api_youtube_object($account_id, $by_name);

  try {
    $youTubePlaylist = new Google_Playlist();

    $parts = array();
    // This code will create a new, private playlist in the authorized user's channel and
    // add the video to the playlist.
    // 1. Create the PlaylistSnippet and add the required data.
    if (array_key_exists('snippet', $params)) {
      $attributes = youtube_playlist_snippet_attributes();
      // Clean array before passing it if user has passed any unwanted indexes.
      $params['snippet'] = array_intersect_key($params['snippet'], $attributes);
      $playlistSnippet = new Google_PlaylistSnippet();
      foreach ($params['snippet'] as $attr => $val) {
        $playlistSnippet->$attributes[$attr]($val);
      }
      $youTubePlaylist->setSnippet($playlistSnippet);
      $parts[] = 'snippet';
    }

    if (array_key_exists('status', $params)) {
      $attributes = youtube_playlist_status_attributes();
      // Clean array before passing it if user has passed any unwanted indexes.
      $params['status'] = array_intersect_key($params['status'], $attributes);
      $playlistStatus = new Google_PlaylistStatus();
      foreach ($params['status'] as $attr => $val) {
        $playlistStatus->$attributes[$attr]($val);
      }
      $youTubePlaylist->setStatus($playlistStatus);
      $parts[] = 'status';
    }

    $parts = implode(',', $parts);
    // 4. Execute the request and return an object containing information about the new playlist
    return $youtube->playlists->insert($parts, $youTubePlaylist, array());


  } catch (Google_ServiceException $e) {
    watchdog('youtube_api', t('A service error occurred: :code', array(':code' => $e->getMessage())));
  } catch (Google_Exception $e) {
    watchdog('youtube_api', t('A client error occurred: :code', array(':code' => $e->getMessage())));
  }
  return FALSE;
}

function youtube_api_playlist_list($filter = array('mine' => TRUE), $parts = array('snippet'), $account_id, $by_name = TRUE) {
  $youtube = youtube_api_youtube_object($account_id, $by_name);
  try {
    // Create a playlist list request
    $listResponse = $youtube->playlists->listPlaylists(implode(',', $parts), $filter);

    $playLists = $listResponse['items'];
    if (empty($playLists)) {
      watchdog('youtube_api', t('Can not find a playlist with id: :video_id', array(':video_id' => $videoId)));
    } else {
      // Since a unique video id is given, it will only return 1 video.
      if (array_key_exists('id', $filter)) {
        return $playLists[0];
      } else {
        return $playLists;
      }
    }
  } catch (Google_ServiceException $e) {
    watchdog('youtube_api', t('A service error occurred: :code', array(':code' => $e->getMessage())));
  } catch (Google_Exception $e) {
    watchdog('youtube_api', t('A client error occurred: :code', array(':code' => $e->getMessage())));
  }
  return FALSE;
}

/*


You must specify a value for these properties:

    id
    snippet.title

You can set values for these properties:

    snippet.title
    snippet.description
    status.privacyStatus
    snippet.tags[]

*/
function youtube_api_playlist_update($id, $params, $account_id, $by_name = TRUE) {
  $youtube = youtube_api_youtube_object($account_id, $by_name);

  try {

    $playlist = youtube_api_playlist_list(array('id' => $id), array('snippet', 'status'));

    $youTubePlaylist = new Google_Playlist($playlist);

    $snippet = array_key_exists('snippet', $params) ? $params['snippet'] : array();

    $snippet = array_merge($playlist['snippet'], $snippet);

    $youTubePlaylist->setSnippet(new Google_PlaylistSnippet($snippet)); 

    $status = array_key_exists('status', $params) ? $params['status'] : array();

    $status = array_merge($playlist['status'], $status);

    $youTubePlaylist->setStatus(new Google_PlaylistStatus($status)); 

    // 4. Execute the request and return an object containing information about the new playlist
    return $youtube->playlists->update('snippet,status', $youTubePlaylist);


  } catch (Google_ServiceException $e) {
    watchdog('youtube_api', t('A service error occurred: :code', array(':code' => $e->getMessage())));
  } catch (Google_Exception $e) {
    watchdog('youtube_api', t('A client error occurred: :code', array(':code' => $e->getMessage())));
  }
  return FALSE;
}

function youtube_api_playlist_delete($id, $account_id, $by_name = TRUE) {
  $youtube = youtube_api_youtube_object($account_id, $by_name);
  try {
    // Create a video list request
    return $youtube->playlists->delete($id);
  } catch (Google_ServiceException $e) {
    watchdog('youtube_api', t('A service error occurred: :code', array(':code' => $e->getMessage())));
  } catch (Google_Exception $e) {
    watchdog('youtube_api', t('A client error occurred: :code', array(':code' => $e->getMessage())));
  }
  return FALSE;
}

function youtube_api_playlist_item_list($filters, $params, $parts = array('snippet'), $account_id, $by_name = TRUE) {
  $youtube = youtube_api_youtube_object($account_id, $by_name);
  try {
    // Create a video list request
    $parts = implode(',', $parts);
    $options = array_merge($filters, $params);
    return $youtube->playlistItems->listPlaylistItems($parts, $options);
  } catch (Google_ServiceException $e) {
    watchdog('youtube_api', t('A service error occurred: :code', array(':code' => $e->getMessage())));
  } catch (Google_Exception $e) {
    watchdog('youtube_api', t('A client error occurred: :code', array(':code' => $e->getMessage())));
  }
  return FALSE;
}

/*
You must specify a value for these properties:

    snippet.playlistId
    snippet.resourceId

You can set values for these properties:

    snippet.playlistId
    snippet.position
    snippet.resourceId
    contentDetails.note
    contentDetails.startAt
    contentDetails.endAt

*/
function youtube_api_playlist_item_insert($video_id, $playlist_id, $params, $account_id, $by_name = TRUE) {
  $youtube = youtube_api_youtube_object($account_id, $by_name);
  try {

    $parts[] = 'snippet';

    $resourceId = new Google_ResourceId();
    $resourceId->setVideoId($video_id);
    $resourceId->setKind('youtube#video');

    $playlistItem = new Google_PlaylistItem();
    $snippet = array_key_exists('snippet', $params) ? $params['snippet'] : array();
    $playlistItemSnippet = new Google_PlaylistItemSnippet($snippet);
    $playlistItemSnippet->setPlaylistId($playlist_id);
    $playlistItemSnippet->setResourceId($resourceId);

    $content_details = array_key_exists('contentDetails', $params) ? $params['contentDetails'] : array();

    if (count($content_details)) {
      $playlistItem->setContentDetails(new Google_PlaylistItemContentDetails($content_details));
      $parts[] = 'contentDetails';
    }

    $parts = implode(',', $parts);

    return $youtube->playlistItems->insert($parts, $playListItem);
  } catch (Google_ServiceException $e) {
    watchdog('youtube_api', t('A service error occurred: :code', array(':code' => $e->getMessage())));
  } catch (Google_Exception $e) {
    watchdog('youtube_api', t('A client error occurred: :code', array(':code' => $e->getMessage())));
  }
  return FALSE;
}

function youtube_api_playlist_get($search, $account_id, $by_name = TRUE) {
  $params = array(
    'channelId' => $channelId,
    'type'      => 'playlist',
  );

  $results = youtube_api_search($search, $params, $account_id, $by_name = TRUE);
  return $results['playlists'];
}

function youtube_api_search($search, $params, $account_id, $by_name = TRUE) {
  $youtube = youtube_api_youtube_object($account_id, $by_name);
  try {
    $params += array(
      'q' => $search,
    );
    $searchResponse = $youtube->search->listSearch('id,snippet', $params); 

    $videos = array();
    $channels = array();
    $playlists = array();

    foreach ($searchResponse['items'] as $searchResult) {
      switch ($searchResult['id']['kind']) {
        case 'youtube#video':
          $videos[] = $searchResult['id']['videoId'];
          break;
        case 'youtube#channel':
          $channels[] = $searchResult['id']['channelId'];
          break;
        case 'youtube#playlist':
          $playlists[] = $searchResult['id']['playlistId'];
          break;
      }
    }

    return array('playlists' => $playlists, 'channels' => $channels, 'videos' => $videos);

  } catch (Google_ServiceException $e) {
    $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
      htmlspecialchars($e->getMessage()));
  } catch (Google_Exception $e) {
    $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
      htmlspecialchars($e->getMessage()));
  }
}
